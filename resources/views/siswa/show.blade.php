@extends('layouts.temadmin')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Data Siswa
                    </div>
                    <div class="card-body">
                        <div class="mb-3">
                            <label class="form-label">Nama Siswa</label>
                            <input type="text" class="form-control " name="nama" value="{{ $siswa->nama }}" readonly>
                            <br>
                            <div class="mb-3">
                                <label class="form-label">Kota/Kabupaten</label>
                                <input type="text" class="form-control" name="kota_id" value="{{ $siswa->kota->kota }}" readonly>
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Kecamatan</label>
                                <input type="text" class="form-control" name="kecamatan_id" value="{{ $siswa->kecamatan->kecamatan }}" readonly>
                            </div>
                            <div class="mb-3">
                                <label class="form-label">Alamat</label>
                                <input type="text" class="form-control" name="alamat" value="{{ $siswa->alamat }}" readonly>
                            </div>
                     
                          
                                <a href="{{ route('siswa.index') }}" class="btn btn-danger" type="submit">Kembali</a>
                           
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection