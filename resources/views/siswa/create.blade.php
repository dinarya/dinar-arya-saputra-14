@extends('layouts.temadmin')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @include('layouts/_flash')
                <div class="card">
                    <div class="card-header">
                        Data Siswa
                    </div>
                    <div class="card-body">
                        <form action="{{ route('siswa.store') }}" method="post">
                            @csrf
                            <div class="row mb-3">
                                <label class="col-sm-2 col-form-label" for="">Nama</label>
                                <div class="col-sm-10">
                                    <div class="input-group input-group-merge">
                                <input type="text" class="form-control  @error('nama') is-invalid @enderror"
                                    name="nama">
                                @error('nama')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-sm-2 col-form-label" for="basic-icon-default-fullname">Kota/Kabupaten</label>
                                <div class="col-sm-10">
                                    <div class="input-group input-group-merge">
                                <select name="kota_id"  id="kota"
                                 class="form-control @error('kota_id') is-invalid @enderror">
                                    @foreach ($kotas as $kota)
                                    <option value="" hidden>pilih Kota/Kabupaten
                                    <option value="{{ $kota->id }}">{{ $kota->kota }}
                                    </option>
                                    @endforeach
                                </select>
                                @error('kota_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            </div>
                            </div>
                         
                            <div class="row mb-3">
                                <label class="col-sm-2 col-form-label" for="basic-icon-default-fullname">Kecamatan</label>
                                <div class="col-sm-10">
                                    <div class="input-group input-group-merge">
                                <select name="kecamatan_id" id="kecamatan" 
                                class="form-control @error('kecamatan_id') is-invalid @enderror">
                                    {{-- @foreach ($kel_labs as $kecamatan) --}}
                                    <option value="" hidden>Pilih Kota/Kabupaten dulu
                                    </option>
                                    {{-- @endforeach --}}
                                </select>
                                @error('kecamatan_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-sm-2 col-form-label" for="basic-icon-default-fullname">Alamat</label>
                                <div class="col-sm-10">
                                    <div class="input-group input-group-merge">
                                <textarea class="form-control  @error('alamat') is-invalid @enderror" name="alamat"></textarea>
                                @error('alamat')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            </div>
                            </div>
                          
                            <div class="row justify-content-end">
                                <div class="col-sm-10">
                                  <button type="submit" class="btn btn-primary">Save</button>
                                </div>
                              </div>
                              
                        
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script> 
    <script>
        $(document).ready(function() {
            $('#kota').on('change', function() {
                var kota_id = $(this).val();
                if (kota_id) {
                    $.ajax({
                        url: '/getkel/' + kota_id,
                        type: "GET",
                        data: {
                            "_token": "{{ csrf_token() }}"
                        },
                        dataType: "json",
                        success: function(data) {
                            if (data) {
                                $('#kecamatan').empty();
                                $('#kecamatan').append(
                                    '<option hidden>Pilih Kota/Kabupaten</option>');
                                $.each(data, function(key, kecamatan) {
                                    $('select[name="kecamatan_id"]').append(
                                        '<option value="' + kecamatan.id + '">' +
                                        kecamatan.kecamatan + '</option>');
                                });
                            } else {
                                $('#kecamatan').empty();
                            }
                        }
                    });
                } else {
                    $('#kecamatan').empty();
                }
            });
        });
    </script>