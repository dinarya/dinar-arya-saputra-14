@extends('layouts.temadmin')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @include('layouts._flash')
                <div class="card border-secondary">
                    <div class="card-header mb-3">Data Siswa</div>

                    <div class="card-body">
                        <form action="{{ route('siswa.update', $siswa->id) }}" method="post">
                            @method('put')
                            @csrf
                            <div class="mb-3">
                                <label for="">Nama</label>
                                <input type="text" name="nama"
                                    class="form-control @error('nama') is-invalid @enderror" value="{{ $siswa->nama }}">
                                </input>
                                @error('nama')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                          
                           
                            <div class="mb-1">
                                <label for="">Kota/Kabupaten</label>
                                <select name="kota_id" id="kota"
                                    class="form-control @error('kota_id') is-invalid @enderror">
                                    @foreach ($kotas as $kota)
                                        <option value="" hidden>pilih Lantai
                                        <option value="{{ $kota->id }}">{{ $kota->kota }}
                                        </option>
                                    @endforeach
                                </select>
                                @error('kota_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="mb-1">
                                <label for="">Kecamatan</label>
                                <select name="kecamatan_id" id="kecamatan"
                                    class="form-control @error('kecamatan_id') is-invalid @enderror">
                                    {{-- @foreach ($kel_labs as $kecamatan) --}}
                                    <option value="" hidden>Pilih Kota/Kabupaten lebih dulu
                                    </option>
                                    {{-- @endforeach --}}
                                </select>
                                @error('kecamatan_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="mb-3">
                                <label for="">Alamat</label>
                                <input type="text" name="alamat"
                                    class="form-control @error('alamat') is-invalid @enderror" value="{{ $siswa->alamat }}">
                                </input>
                                @error('alamat')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                           
                                    <button class="btn btn-primary" type="submit">Kirim</button>
                                    {{-- <a href="{{ route('siswa.index') }}" class="btn btn-danger"
                                        type="submit">Kembali</a>
                                </div> --}}
                                {{-- </div>
                        <div class="mb-3">
                            <div class="d-grid gap-2">

                            </div>
                        </div> --}}
                        </form>
                   
            </div>
        </div>
    </div>
@endsection

<script src="https://code.jquery.com/jquery-3.6.0.min.js"
    integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
<script>
    $(document).ready(function() {
        $('#kota').on('change', function() {
            var kota_id = $(this).val();
            if (kota_id) {
                $.ajax({
                    url: '/getkel/' + kota_id,
                    type: "GET",
                    data: {
                        "_token": "{{ csrf_token() }}"
                    },
                    dataType: "json",
                    success: function(data) {
                        if (data) {
                            $('#kecamatan').empty();
                            $('#kecamatan').append(
                                '<option hidden>Pilih Kecamatan</option>');
                            $.each(data, function(key, kecamatan) {
                                $('select[name="kecamatan_id"]').append(
                                    '<option value="' + kecamatan.id + '">' +
                                    kecamatan.kecamatan + '</option>');
                            });
                        } else {
                            $('#kecamatan').empty();
                        }
                    }
                });
            } else {
                $('#kecamatan').empty();
            }
        });
    });
</script>
