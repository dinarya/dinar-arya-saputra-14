@extends('layouts.temadmin')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @include('layouts/_flash')
                <div class="card">
                    <div class="card-header">
                        Data Kota/Kabupaten
                    </div>
                    <div class="card-body">
                        <form action="{{ route('kota.update', $kota->id) }}" method="post">
                            @csrf
                            @method('put')
                            <div class="mb-3">
                                <label class="form-label">Kota Kabupaten</label>
                                <input type="text" class="form-control  @error('kota') is-invalid @enderror"
                                    name="kota" value="{{ $kota->kota }}">
                                @error('kota')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                  
                      
                                    <button class="btn btn-primary" type="submit">Save</button>
                               
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection