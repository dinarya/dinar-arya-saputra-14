@extends('layouts.temadmin')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Data Kota/Kabupaten
                    </div>
                    <div class="card-body">
                        <div class="mb-3">
                            <label class="form-label">Kota/Kabupaten</label>
                            <input type="text" class="form-control " name="kota" value="{{ $kota->kota }}" readonly>
                        </div>
                      
                     
                                <a href="{{ route('kota.index') }}" class="btn btn-primary" type="submit">Kembali</a>
                            
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection