@extends('layouts.temadmin')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        Data Kecamatan
                    </div>
                    <div class="card-body">
                        <div class="mb-3">
                            <label class="form-label">Kecamatan</label>
                            <input type="text" class="form-control " name="kecamatan" value="{{ $kecamatan->kecamatan }}" readonly>
                        <br>
                            <div class="mb-3">
                                <label class="form-label">Kota/Kabupaten</label>
                                <input type="text" class="form-control" name="kota_id" value="{{ $kecamatan->kota->kota }}" readonly>
                            </div>
                      
                                <a href="{{ route('kecamatan.index') }}" class="btn btn-primary" type="submit">Kembali</a>
                          
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection