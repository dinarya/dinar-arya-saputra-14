@extends('layouts.temadmin')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @include('layouts/_flash')
                <div class="card">
                    <div class="card-header">
                        Data Kecamatan
                    </div>
                    <div class="card-body">
                        <form action="{{ route('kecamatan.store') }}" method="post">
                            @csrf
                            <div class="mb-3">
                                <label class="form-label">Kecamatan</label>
                                <input type="text" class="form-control  @error('kecamatan') is-invalid @enderror"
                                    name="kecamatan">
                                @error('kecamatan')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="mb-1">
                                <label for="">Kota/Kabupaten</label>
                                <select name="kota_id" id="kota"
                                    class="form-control @error('kota_id') is-invalid @enderror">
                                    @foreach ($kecamatans as $kota)
                                        <option value="" hidden>Pilih Kota/Kabupaten
                                        <option value="{{ $kota->id }}">{{ $kota->kota }}
                                        </option>
                                    @endforeach
                                </select>
                                @error('kota_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                           
                          <br>
                                    <button class="btn btn-primary" type="submit">Save</button>
                             
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection