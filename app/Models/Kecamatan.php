<?php

namespace App\Models;

use App\Models\Siswa;
use App\Models\kota;
use Illuminate\Database\Eloquent\Factories\HasFactory;
// use App\Models\Kel_lab;
use Illuminate\Database\Eloquent\Model;

class Kecamatan extends Model
{
    use HasFactory;
    // public $fillable = ['no_lab', 'kondisi'];

    // membuat fitur created_at(kapan data dibuat) & updated_at (kapan data diedit)
    // aktif
    public $guarded = [];
    public $timestamps = true;
    public $table = 'kecamatans';

    public function kota()
    {
        return $this->belongsTo(kota::class, 'kota_id');
    }
    public function Siswa()
    {
        return $this->hasMany(Siswa::class, 'id');
    }

}
