<?php

namespace App\Models;

use App\Models\Siswa;
use App\Models\Kecamatan;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kota extends Model
{
    use HasFactory;
   
    protected $table = 'kotas';
    public function kecamatan()
    {
        return $this->hasMany(Kecamatan::class);
    }
    public function Siswa()
    {
        return $this->hasMany(Siswa::class, 'id_siswas');
    }
}
