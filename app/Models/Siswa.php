<?php

namespace App\Models;

use App\Models\Kecamatan;
use App\Models\Kota;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    use HasFactory;
    // public $fillable = ['nama','hari','tanggal','jam','kota','no_lab','kegiatan','tanggal_berakhir','keterangan'];
    // membuat fitur created_at(kapan data dibuat) & updated_at (kapan data diedit)
    // aktif
    public $guarded = ['id'];

    public function kota()
    {
        return $this->belongsTo(Kota::class, 'kota_id');
    }
    public function kecamatan()
    {
        return $this->belongsTo(Kecamatan::class, 'kecamatan_id');
    }

}
