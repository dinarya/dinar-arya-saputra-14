<?php

namespace App\Http\Controllers;

use App\Models\Kecamatan;
use App\Models\Kota;
// use App\Models\Jad_lab;
use Illuminate\Http\Request;

class KecamatanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      
        $kota = Kota::all();
        $kecamatan = Kecamatan::all();
        return view('kecamatan.index', compact('kecamatan', 'kota'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kecamatans = Kecamatan::all();
        $kecamatans = Kota::all();
        return view('kecamatan.create', compact('kecamatans'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'kota_id' => 'required',
            'kecamatan' => 'required',
          
        ]);
        $kecamatan = new Kecamatan();
        $kecamatan->kota_id = $request->kota_id;
        $kecamatan->kecamatan = $request->kecamatan;
      
        $kecamatan->save();
        return redirect()->route('kecamatan.index')
            ->with('success', 'Data berhasil dibuat!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kecamatan = Kecamatan::findOrFail($id);
        return view('kecamatan.show', compact('kecamatan'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kecamatan = Kecamatan::findOrFail($id);
        $kota = Kota::all();
        return view('kecamatan.edit', compact('kecamatan', 'kota'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validated = $request->validate([
            'kota_id' => 'required',
            'kecamatan' => 'required',
          
        ]);

        $kecamatan = Kecamatan::findOrFail($id);
        $kecamatan->kota_id = $request->kota_id;
        $kecamatan->kecamatan = $request->kecamatan;
      
        $kecamatan->save();
        return redirect()->route('kecamatan.index')
            ->with('success', 'Data berhasil diedit!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kecamatan = Kecamatan::findOrFail($id);
        $kecamatan->delete();
        return redirect()->route('kecamatan.index')
            ->with('success', 'Data berhasil dihapus!');
    }
    public function getKecamatan($id)
    {
        $kecamatans = Kecamatan::where('kota_id', $id)->get();
        return response()->json($kecamatans);
    }
}
