<?php

namespace App\Http\Controllers;

use App\Models\Siswa;
use App\Models\Kecamatan;
use App\Models\Kota;
use Illuminate\Http\Request;

class SiswaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $kota = Kota::all();
        $siswa = Siswa::with('kecamatan', 'kota')->latest()->get();

        // $siswa = Siswa::all();
        return view('siswa.index', compact('siswa'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kecamatans = Kecamatan::all();
        $kotas = Kota::all();
        return view('siswa.create', compact('kecamatans', 'kotas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'nama' => 'required',
            'kota_id' => 'required',
            'kecamatan_id' => 'required',
            'alamat' => 'required',
        ]);
        $siswa = new Siswa();
        $siswa->nama = $request->nama;
        $siswa->kecamatan_id = $request->kecamatan_id;
        $siswa->kota_id = $request->kota_id;
        $siswa->alamat = $request->alamat;

      

        $siswa->save();
        return redirect()->route('siswa.index')
            ->with('success', 'Data berhasil dibuat!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $siswa = Siswa::findOrFail($id);
        return view('siswa.show', compact('siswa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $siswa = Siswa::findOrFail($id);
        $kecamatans = Kecamatan::where('kota_id', $siswa->kota_id)->get();
        $kotas = Kota::all();
        return view('siswa.edit', compact('siswa', 'kecamatans', 'kotas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validated = $request->validate([
            'kota_id' => 'required',
            'nama' => 'required',
            'kecamatan_id' => 'required',
            'alamat' => 'required',
        ]);

        $siswa = Siswa::findOrFail($id);
        $siswa->nama = $request->nama;
        $siswa->kecamatan_id = $request->kecamatan_id;
        $siswa->kota_id = $request->kota_id;
        $siswa->alamat = $request->alamat;
        $siswa->save();
        return redirect()->route('siswa.index')
            ->with('success', 'Data berhasil diedit!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $siswa = Siswa::findOrFail($id);
        $siswa->delete();
        return redirect()->route('siswa.index')
            ->with('success', 'Data berhasil dihapus!');
    }

}
