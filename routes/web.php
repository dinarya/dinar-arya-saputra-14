<?php

// panggil controller Siswa
use App\Http\Controllers\SiswaController;
use App\Http\Controllers\KotaController;
use App\Http\Controllers\KecamatanController;
use App\Models\Kecamatan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');



// route kota/kabupaten
Route::resource('kota', KotaController::class);
Auth::routes();

// route kecamatan
Route::resource('kecamatan', KecamatanController::class);
Auth::routes();

// route siswa
Route::resource('siswa', SiswaController::class);
Auth::routes();

//dinamic
Route::get('getkel/{id}', function ($id) {
    $kecamatan = Kecamatan::where('kota_id', $id)->get();
    return response()->json($kecamatan);
});